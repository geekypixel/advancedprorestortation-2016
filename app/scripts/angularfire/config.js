angular.module('firebase.config', [])
  .constant('FBURL', 'https://advancedpro.firebaseio.com')
  .constant('SIMPLE_LOGIN_PROVIDERS', ['password','google','twitter'])

  .constant('loginRedirectPath', '/login');
