'use strict';

/**
 * @ngdoc function
 * @name workspaceApp.controller:ArticleCtrl
 * @description
 * # ArticleCtrl
 * Controller of the workspaceApp
 */
angular.module('workspaceApp')
  .controller('ArticleCtrl', function ($scope, $firebaseObject, Ref, $routeParams) {
    
    $scope.article = $firebaseObject(Ref.child('posts/'+$routeParams.bid));
    
    
    $scope.article.$loaded().then(function(post){
            post.image.cropped = 'http://res.cloudinary.com/geekypixel/image/upload/w_1550,h_500,c_fill,g_faces:center/' + post.image.path;
    }); 
    
    
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
