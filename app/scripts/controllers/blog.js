'use strict';

/**
 * @ngdoc function
 * @name workspaceApp.controller:BlogCtrl
 * @description
 * # BlogCtrl
 * Controller of the workspaceApp
 */
angular.module('workspaceApp')
    .controller('BlogCtrl', function ($scope, $firebaseArray, Ref) {

    $scope.posts = $firebaseArray(Ref.child('posts'));
    
    
    $scope.posts.$loaded().then(function(blogs){
        for (var x in blogs) {
            blogs[x].image.cropped = 'http://res.cloudinary.com/geekypixel/image/upload/w_1350,h_640,c_fit/' + blogs[x].image.path;
        }
    });   
    
    
    $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
    ];


});
