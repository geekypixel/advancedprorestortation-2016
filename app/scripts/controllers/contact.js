'use strict';

/**
 * @ngdoc function
 * @name workspaceApp.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the workspaceApp
 */
angular.module('workspaceApp')
    .controller('ContactCtrl', function ($scope, $http) {

    $scope.messageSent = false;

    $scope.contact = function(message) {
        $http({
            method: 'GET',
            url: 'https://pixelmail-60461.onmodulus.net',
            params: { 
                to: 'peter@geekypixel.com', 
                fromname: 'Advanced Pro Restoration', 
                from: 'contact@advancedprorestoration.com', 
                subject: 'New message from a website visitor!', 
                text:  message.author + ' ('+ message.authorEmail +') said:' + message.body
            }
        });
        $scope.messageSent = true;
    };

    $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
    ];
});
