'use strict';

/**
 * @ngdoc function
 * @name workspaceApp.controller:FaqCtrl
 * @description
 * # FaqCtrl
 * Controller of the workspaceApp
 */
angular.module('workspaceApp')
  .controller('FaqCtrl', function ($scope, $firebaseArray, Ref) {
    
    $scope.faq = $firebaseArray(Ref.child('faqs'));
    
    
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
