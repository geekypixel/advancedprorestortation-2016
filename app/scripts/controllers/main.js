'use strict';

/**
 * @ngdoc function
 * @name workspaceApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the workspaceApp
 */
angular.module('workspaceApp')
    .controller('MainCtrl', function ($scope, $firebaseArray, Ref) {


    $scope.blogs = $firebaseArray(Ref.child('posts').limitToLast(3));

    $scope.blogs.$loaded().then(function(blogs){
        for (var x in blogs) {
            blogs[x].image.cropped = 'http://res.cloudinary.com/geekypixel/image/upload/w_381,h_381,c_fill,g_faces:center/' + blogs[x].image.path;

            var yourString = blogs[x].summary; //replace with your string.
            var maxLength = 115; // maximum number of characters to extract

            //trim the string to the maximum length
            var trimmedString = yourString.substr(0, maxLength);

            //re-trim if we are in the middle of a word
            trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));
            
            blogs[x].summary = trimmedString;

        }
    });

    $scope.testimonials = $firebaseArray(Ref.child('testimonials'));

    $scope.testimonials.$loaded().then(function(testimonials){
        $scope.activeTestimonial = testimonials[0];
    });
    

    $scope.selected = 0;



    var index = 0;
    var selected = 0;

    function setActiveTestimonial() {
        index = (index + 1) % $scope.testimonials.length;
        selected = (selected + 1) % $scope.testimonials.length;
        $scope.activeTestimonial = $scope.testimonials[index];

    };

    $scope.setActiveTestimonialNext = function() {

        index = (index + 1) % $scope.testimonials.length;
        selected = (selected + 1) % $scope.testimonials.length;
        $scope.activeTestimonial = $scope.testimonials[index];
        $scope.selected = selected; 
        if(selected === 10) {
            $scope.activeTestimonial = $scope.testimonials[0];
        }
    };

    $scope.setActiveTestimonialPrev = function() {
        if(selected === 0) {
            return;
        } else {
            index = (index - 1) % $scope.testimonials.length;
            selected = (selected - 1) % $scope.testimonials.length;
            $scope.activeTestimonial = $scope.testimonials[index];
            $scope.selected = selected; 
        }
    };


    $scope.swapTestimonial = function(t) {
        var selectedIndex = $scope.testimonials.indexOf(t);
        $scope.activeTestimonial = $scope.testimonials[selectedIndex];
        $scope.selected = selectedIndex; 
    };




    $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
    ];
});
