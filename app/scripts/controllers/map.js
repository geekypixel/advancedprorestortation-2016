'use strict';

/**
 * @ngdoc function
 * @name workspaceApp.controller:TrackCtrl
 * @description
 * # TrackCtrl
 * Controller of the workspaceApp
 */
angular.module('workspaceApp')
    .controller('MapCtrl', function ($scope, $firebaseObject, Ref, $rootScope, $interval, $http) {



    $scope.job = $firebaseObject(Ref.child('jobs/-KGD2xtisVfEI-O9USoq'));

    $scope.job.$loaded().then(function(job){


        // you can specify the default lat long
        var map,
            currentPositionMarker,
            mapCenter = new google.maps.LatLng(14.668626, 121.24295),
            map;


      

        // change the zoom if you want
        function initializeMap()
        {
            map = new google.maps.Map(document.getElementById('map_canvas'), {
                zoom: 15,
                center: mapCenter,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
        }

        function locError(error) {
            // tell the user if the current position could not be located
            alert("The current position could not be found!");
        }




        // current position of the user
        function setCurrentPosition(pos) {
            $rootScope.myPos = pos;

            currentPositionMarker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(
                    pos.coords.latitude,
                    pos.coords.longitude
                ),
                title: "Current Position"
            });


            map.panTo(new google.maps.LatLng(
                pos.coords.latitude,
                pos.coords.longitude
            ));

            //geocode address

            var sanitizedDestination = job.address.split(' ').join('+');
            $http({
                method: 'GET',
                url: 'https://maps.googleapis.com/maps/api/geocode/json?address='+sanitizedDestination
            }).then(function(result) {
                $scope.targetDest = result.data.results[0].geometry.location;
                console.log(result.data.results[0].geometry.location);


                var destLong = Number(result.data.results[0].geometry.location.lng);
                var destLat = Number(result.data.results[0].geometry.location.lat);

                $scope.userLong = Number($rootScope.myPos.coords.longitude);
                $scope.userLat = Number($rootScope.myPos.coords.latitude);

                var directionsService = new google.maps.DirectionsService();
                var request = {
                    origin:  {lat: $scope.userLat, lng: $scope.userLong}, 
                    destination: {lat: destLat, lng: destLong},
                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                };
                directionsService.route(request, function(response, status) {
                    if (status === google.maps.DirectionsStatus.OK) {
                        $scope.duration = response.routes[0].legs[0].duration.text;
                    } else {
                        window.alert('Directions request failed due to ' + status);
                    }
                    $interval(function () {
                        logCoords($rootScope.myPos);
                    }, 1000);
                    $scope.$apply();

                });



            });





        }




        function displayAndWatch(position) {

            // set current position
            setCurrentPosition(position);

            // watch position
            watchCurrentPosition();


            map.panTo(position);



        }




        function watchCurrentPosition() {
            var positionTimer = navigator.geolocation.watchPosition(

                function (position) {
                    setMarkerPosition(
                        currentPositionMarker,
                        position
                    );

                });

        }

        function setMarkerPosition(marker, position) {
            marker.setPosition(
                new google.maps.LatLng(
                    position.coords.latitude,
                    position.coords.longitude)
            );
            map.panTo(new google.maps.LatLng(
                position.coords.latitude,
                position.coords.longitude
            ));


        }


        function initLocationProcedure() {
            initializeMap();
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(displayAndWatch, locError);
            } else {
                // tell the user if a browser doesn't support this amazing API
                alert("Your browser does not support the Geolocation API!");
            }
        }


        // initialize with a little help of jQuery
        $(document).ready(function() {
            initLocationProcedure();
        });


        //debug
        $scope.jobs = [
            { address: '817 S Main Street, Las Vegas, NV 89101', customerName: 'Geeky Pixel', jobType: 'Flood' },
            { address: '3600 Las Vegas Blvd, Las Vegas, NV 89135', customerName: 'Bellagio', jobType: 'Fire' },
            { address: '11011 W Charleston Blvd, Las Vegas, NV 89135', customerName: 'John Smith', jobType: 'Constructoin' },
        ];

        $scope.targetDest = $scope.jobs[0].address;
        $scope.currentJob = $scope.jobs[0];


        $scope.activeJob = function(j) {
            $scope.targetDest = j.address;
            setCurrentPosition($rootScope.myPos); 
            $scope.currentJob = j;

            $http({
                method: 'GET',
                url: 'https://pixelmail-60461.onmodulus.net',
                params: { to: 'peter@geekypixel.com', fromname: 'Advanced Pro Restoration', from: 'realtime@advancedprorestoration.com', subject: 'A technician is on the way!', text: 'Please visit https://advancedpro.firebaseapp.com/map.html to track our technician\'s progress and travel time to your property.' }
            });


        };

    });





    $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
    ];
});
